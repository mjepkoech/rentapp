package com.RentApplication;

import java.net.HttpURLConnection;
import java.util.HashSet;
import java.util.Set;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;

//import io.vertx.reactivex.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.impl.RequestParametersImpl;
import io.vertx.ext.web.api.validation.HTTPRequestValidationHandler;
import io.vertx.ext.web.api.validation.ParameterType;
import io.vertx.ext.web.handler.CorsHandler;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.Future;
import lombok.experimental.var;
import org.omg.CORBA.SystemException;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.KeyStoreOptions;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
/**
 * 
 * This is the gateway class
 * 
 *
 */
public class APIGateway extends AbstractVerticle {

    private static final String HEADER_AUTH = "Authorization";

    private static final String AUTH_PREFIX = "Bearer ";

    private final String userKey = "userId";

    JWTAuthOptions jwtAuthOptions = new JWTAuthOptions();


    private static final Logger logger = LoggerFactory.getLogger(APIGateway.class);
    private static JWTAuth jwtauth = null;

	@Override
	public void start() throws Exception {
		System.out.println("deploymentId =" + vertx.getOrCreateContext().deploymentID());
		int port = 7453;

		Router router = Router.router(vertx);
		HTTPRequestValidationHandler validationHandler = HTTPRequestValidationHandler.create().addPathParam("parameter",
				ParameterType.GENERIC_STRING);

		Set<String> allowedHeaders = new HashSet<>();
		allowedHeaders.add("x-requested-with");
		allowedHeaders.add("Access-Control-Allow-Origin");
		allowedHeaders.add("Access-Control-Allow-Headers");
		allowedHeaders.add("Access-Control-Allow-Methods");
		allowedHeaders.add("origin");
		allowedHeaders.add("Content-Type");
        allowedHeaders.add("Authorization");
		allowedHeaders.add("accept");
		allowedHeaders.add("X-PINGARUNER");


		router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethod(HttpMethod.POST)
				.allowedMethod(HttpMethod.GET));

        router.post("/login").handler(this::loginEntryHandler);
        router.post("/tenant").handler(this::registerEntryHandler);
        router.post("/:parameter").handler(validationHandler).handler(this::callOperations);

        HttpServer server = vertx.createHttpServer();
		server.requestHandler(router::accept);
		server.listen(port,  res -> {
			if (res.succeeded()) {
				System.out.println("ESB Rest API Server is now listening at :" + port);
			} else {
				System.out.println("Failed to bind ESB Rest API!");
			}
		});
	}

    private void loginEntryHandler(RoutingContext routing) {
//        String authtoken = routing.request().headers().get("Authorization");
//        System.out.println("Authorization details" + authtoken);
		routing.request().bodyHandler(request -> {
            String path = "login";
			JsonObject body = request.toJsonObject();

			HttpServerResponse response = routing.response().putHeader("Content-Type", "application/json");
			try {
                authenticateChannel(path, body, response);
            } catch (Exception ex) {
                response.end(ex.getMessage());
            }

        });
    }

    private void registerEntryHandler(RoutingContext routing) {
//        String authtoken = routing.request().headers().get("Authorization");
//        System.out.println("Authorization details" + authtoken);
        routing.request().bodyHandler(request -> {
            String path = "tenant";
            JsonObject body = request.toJsonObject();

            HttpServerResponse response = routing.response().putHeader("Content-Type", "application/json");
            try {
                authenticateChannel(path, body, response);
            } catch (Exception ex) {
                response.end(ex.getMessage());
            }

        });
    }

    private void authenticateChannel(String path, JsonObject body, HttpServerResponse response) {
        JsonObject data = body;

        // 1. AUTHENTICATE CHANNEL

        EventBus esbBus = vertx.eventBus();
        DeliveryOptions options = new DeliveryOptions();
        int time = 5000;
        options.setSendTimeout(time);
        MessageProducer<JsonObject> producer = esbBus.publisher("AUTHAPI", options);

        producer.send(data, (AsyncResult<Message<JsonObject>> artxn) -> {
            if (artxn.succeeded()) {
                String responseFields = artxn.result().body().toString();
                System.out.println("Received reply from auth Mgr: " + responseFields);

                getMicroservice(path, response, data, esbBus, options);

            } else {
                String f48 = artxn.cause().getMessage();
                String f39 = "57";
                data.put("39", f39);
                data.put("48", f48);

                response.end(data.toString());
                response.close();

            }

		});
	}

    private void getMicroservice(String path, HttpServerResponse response, JsonObject data, EventBus esbBus, DeliveryOptions options) {
        MessageProducer<JsonObject> producerOps = esbBus.publisher(path, options);

        producerOps.send(data, (AsyncResult<Message<JsonObject>> artxnOps) -> {

            if (artxnOps.succeeded()) {
                JsonObject responseDataOps = artxnOps.result().body();
                System.out.println("Received reply from Operation Microservices: " + responseDataOps);

                String f39 = "1";
                responseDataOps.put("status", f39);

                response.end(responseDataOps.toString());
                response.close();

            } else {

                JsonObject feedback = new JsonObject();
                JsonObject params = new JsonObject();

                String f48 = artxnOps.cause().getMessage();
                String f57 = "0";
                feedback.put("status", f57);
                params.put("message", f48);
                feedback.put("data", params);

                response.end(feedback.toString());
                response.close();

            }
        });
    }

    private void callOperations(RoutingContext routing) {

        routing.request().bodyHandler(request -> {

            RequestParametersImpl urlparams = routing.get("parsedParameters");
            String path = urlparams.pathParameter("parameter").toString();
            JsonObject body = request.toJsonObject();

            HttpServerResponse response = routing.response().putHeader("Content-Type", "application/json");
            try {
                if (routing.request().headers().contains("Authorization")) {
                    // authentication object present, send to verify token
                    routing.setBody(Buffer.buffer(request.toString()));
                    verifyToken(routing);
                } else {
                    // authentication object missing
                    routing.response().putHeader("CONTENT-TYPE", CONTENT_TYPE)
                            .setStatusCode(HttpURLConnection.HTTP_UNAUTHORIZED)
                            .end(new JsonObject().put("status", "failed")
                                    .put("message", "Could not load authorization object").put("data", new JsonObject())
                                    .toString());
                }
                authenticateChannel(path, body, response);

			} catch (Exception ex) {
				response.end(ex.getMessage());
			}

		});
	}

    /**
     * authenticate token provided in header
     *
     * @param routingcontext
     */
    private void verifyToken(RoutingContext routingcontext) {
        String authtoken = routingcontext.request().headers().get("Authorization");
        System.out.println("The authentication token is: " + authtoken);
        auth(authtoken);


//		JsonObject authinfo = new JsonObject().put("jwt", authtoken);
//        logger.info("authenticating jwt " + authinfo);
//        try {
//			jwtauth.authenticate(new JsonObject().put("jwt", authtoken), ar -> {
//				if (ar.succeeded()) {
//					System.out.println("Token is valid");
//					JsonObject principal = ar.result().principal();
//					if (principal.containsKey(userKey)) {
//						jwtauth.complete(principal);
//					} else {
//						System.out.println("Token is not valid");
//					SystemException systemException = SystemException.create(DefaultErrorCode.INVALID_TOKEN)
//							.set("details", "no " + userKey);
//					authFuture.fail(systemException);
//					}
//
//				} else {
//					System.out.println("Token is not valid");
//
////					fail(authFuture, ar);
//				}
//			});
//			jwtauth.authenticate(authinfo).onComplete{
//				case Success(result) => {
//					var theUser = result
//				}
//				case Failure(cause) => {
//					println("$cause")
//				}
//			}
//			jwtauth.authenticate(authinfo) function (res, res_err) {
//				if (res_err == null) {
//					var theUser = res;
//				} else {
//					// Failed!
//				}
//			});
//			jwtauth.authenticate(authinfo).subscribe(
//			jwtauth.authenticate(authinfo, ar -> {
//				if (ar.succeeded()) {
//					JsonObject principal = ar.result().principal();
//					if (principal.containsKey(userKey)) {
//						authFuture.complete(principal);
//					} else {
//						SystemException systemException = SystemException.create(DefaultErrorCode.INVALID_TOKEN)
//								.set("details", "no " + userKey);
//						authFuture.fail(systemException);
//					}
//
//				} else {
//					fail(authFuture, ar);
//				}
//					(authvalid) -> {
//						// return the user object from token
////            logger.info("valid jwt " + authvalid.principal());
//						System.out.println("The token is valid");
//						/*
//						 * for valid token, check if the token matches with one stored in database for
//						 * the same user
//						 */
////            usertokenmessage.body().put("user", authvalid.principal());
////            checkjwtagainstDB(usertokenmessage);
//					}, (authfailed) -> {
//						logger.error("invalid jwt " + authfailed.getMessage(), authfailed);
////            usertokenmessage.fail(401, authfailed.getMessage());
//					});
//		}
//        catch (Exception ex){
//			System.out.println(ex);
//
//		};
//		EventBus esbBus = vertx.eventBus();
//		String path = "verifyToken";
//		DeliveryOptions options = new DeliveryOptions();
//		MessageProducer<JsonObject> producerOps = esbBus.publisher(path, options);
    }


    /**
     * take the validated jwt and the extracted user and check against token stored
     * for the same user in the database
     *
     * @param message
     */
//	private void checkjwtagainstDB(Message<JsonObject> message) {
//
//		EventBus esbBus = vertx.eventBus();
//
//		String operation = "add";
//		String funcSP = "sp_loginToken";
//
//		new DatabaseService().callDatabase(funcSP, fields, operation, message, esbBus);
//
//	}
    private Future<JsonObject> auth(String token) {
        Future<JsonObject> authFuture = Future.future();
        JWTAuth provider = JWTAuth.create(vertx, jwtAuthOptions);

        System.out.println(token);

        provider.authenticate(new JsonObject().put("jwt", token), ar -> {
            if (ar.succeeded()) {
                JsonObject principal = ar.result().principal();
//				if (principal.containsKey(userKey)) {
                authFuture.complete(principal);
//				} else {
                System.out.println("Token is valid");
//				}

            } else {
                fail(authFuture, ar);
            }
        });
        return authFuture;
    }

    private void fail(Future<JsonObject> completeFuture, AsyncResult<User> ar) {
        String errorMessage = ar.cause().getMessage();
        if (errorMessage != null) {
            if (errorMessage.startsWith("Expired JWT")) {
                System.out.println("Expired token");
            } else if (errorMessage.startsWith("Invalid JWT")) {
                System.out.println("Invalid token");
            } else {
                System.out.println(errorMessage);
                System.out.println("PERMISSION DENIED");
            }
        } else {
            System.out.println("PERMISSION_DENIED");
        }
    }
}
