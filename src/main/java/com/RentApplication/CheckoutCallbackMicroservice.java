package com.RentApplication;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Class handling mpesa callbacks from the payment gateway
 * @author mjepkoech
 *
 */
public class CheckoutCallbackMicroservice extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		EventBus eb = vertx.eventBus();

		MessageConsumer<JsonObject> consumer = eb.consumer("mpesaCheckoutCallback");
		consumer.handler((Message<JsonObject> message) -> {

			JsonObject reqdata = message.body();
			String operation = "add";

			System.out.println("Received reply from Payment Gateway: " + reqdata);

			try {
//				JsonArray fields = new JsonArray();
				String funcSP = "sp_updateTransaction";

				String id = reqdata.getString("transactionID").trim();
				String status = reqdata.getString("status").trim();
				String status_desc = reqdata.getString("statusDescription").trim();
				String mobile_number = reqdata.getString("sourceAccountNo").trim();


				EventBus esbBus = vertx.eventBus();

				String sms = "You payment has been successfully paid and confirmed" + ".";
				JsonObject smsdata = new JsonObject().put("smsmessage", sms).put("mobilenumber", mobile_number);

				DeliveryOptions options = new DeliveryOptions();
				int time = 5000;
				options.setSendTimeout(time);
				MessageProducer<JsonObject> otpsend = esbBus.publisher("notification", options);
				otpsend.send(smsdata, (AsyncResult<Message<JsonObject>> otpartxn) -> {
					if (otpartxn.succeeded()) {
						JsonObject res = otpartxn.result().body();
						System.out.println("Received reply from SMS Mgr: " + res);

//						new DatabaseService().callDatabase(funcSP, fields, operation, message, esbBus);
					} else {
//						message.fail(0, otpartxn.cause().getMessage());
						System.out.println("Received reply from SMS Mgr: " + otpartxn.cause().getMessage());
					}
					JsonArray fields = new JsonArray().add(id).add(status).add(status_desc).add(mobile_number);

					new DatabaseService().callDatabase(funcSP, fields, operation, message, esbBus);

				});
			} catch (Exception ex) {
				message.fail(0, ex.getMessage());
			}

		});

		consumer.completionHandler(res -> {
			if (res.succeeded()) {
				System.out.println("MpesaCheckout Callback handler has reached all nodes");
			} else {
				System.out.println("MpesaCheckout Callback handler failed!");
			}
		});
	}

}