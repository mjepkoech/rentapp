package com.RentApplication;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.Base64;
import java.io.*;
import java.io.File;

/**
 * Banks CRUD
 *
 * @author mjepkoech
 *
 */

public class Downloads extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		EventBus eb = vertx.eventBus();

	MessageConsumer<JsonObject> consumer = eb.consumer("downloads");
		consumer.handler((Message<JsonObject> message) -> {
			JsonObject reqdata = message.body();
			JsonObject data = new JsonObject();

			System.out.println("Downloads Microservice received a message: " + reqdata.getString("operation") + reqdata.getString("id"));

			try {
				String funcSP = reqdata.getJsonObject("data").getJsonObject("transaction_details").getString("action");

				JsonArray fields = new JsonArray().add(reqdata.getJsonObject("data").getJsonObject("transaction_details").getString("id"));
				DeliveryOptions options = new DeliveryOptions();
				options.setSendTimeout(5000);
				System.out.println("Downloads Microservice received a message: " + reqdata.getString("operation"));

				MessageProducer<JsonObject> producer = eb.publisher("DATABASEACCESS", options);
				producer.send(new JsonObject().put("storedprocedure", funcSP).put("params", fields), (AsyncResult<Message<JsonObject>> artxn) -> {
					JsonObject feedback = new JsonObject();

					if (artxn.succeeded()) {
						JsonObject responseFields = artxn.result().body();
						System.out.println("Received reply from Session Mgr: " + responseFields);

						JsonArray params = responseFields.getJsonArray("params");

						int details_size = params.size();
						String file = "";
						for (int i = 0; i < details_size; i++) {
							JsonObject dbfields = params.getJsonObject(i);
                            file = encoder(dbfields.getString("filepath"));
                            System.out.println("Receive: " + responseFields);

                        }
						data.put("file", file);feedback.put("data", data);
						message.reply(feedback);

					} else {
						System.out.println("Receive: " + "nothing");
						feedback.put("message", "No photos defined");
						feedback.put("status", "1");

//						message.fail(0, artxn.cause().getMessage());
						message.reply(feedback);

					}
				});

			} catch (Exception ex) {
				message.fail(0, ex.getMessage());
			}
	});
	}

    public static String encoder(String filePath) {
        String base64File = "";
        File file = new File(filePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a file from file system
            byte fileData[] = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File = Base64.getEncoder().encodeToString(fileData);
			System.out.println("File found" + base64File);

		} catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file " + ioe);
        }
        return base64File;
    }
}
