package com.RentApplication;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Estates CRUD
 * 
 * @author mjepkoech
 *
 */
public class EstatesMicroService extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		EventBus eb = vertx.eventBus();

		MessageConsumer<JsonObject> consumer = eb.consumer("manageEstates");
		consumer.handler((Message<JsonObject> message) -> {
			JsonObject reqdata = message.body();

			System.out.println("EstatesMicroservice received a message: " + reqdata);
			try {
				String operation = reqdata.getJsonObject("data").getJsonObject("transaction_details")
						.getString("action").trim();
				JsonObject data = reqdata.getJsonObject("data").getJsonObject("transaction_details");

				estateCases(message, operation, data);

			} catch (Exception ex) {
				message.fail(0, ex.getMessage());
			}

		});

		consumer.completionHandler(res -> {
			if (res.succeeded()) {
				System.out.println("Estates handler has reached all nodes");
			} else {
				System.out.println("Estates failed!");
			}
		});
	}

	/**
	 * Method to handle estates operations
	 * @param message
	 * @param operation
	 * @param data
	 */
	private void estateCases(Message<JsonObject> message, String operation, JsonObject data) {
		JsonArray fields = new JsonArray();
		String funcSP = "";

		switch (operation) {
		case "add":
			
			funcSP = "sp_addEstate";
			String name = data.getString("name").trim();
			String street = data.getString("street").trim();
			String town = data.getString("town").trim();
			String landlord = data.getString("landlord").trim();
			fields = new JsonArray().add(name).add(street).add(town).add(landlord);

			break;
		case "update":
			
			funcSP = "sp_updateEstate";
			String id = data.getString("id").trim();
			name = data.getString("name").trim();
			street = data.getString("street").trim();
			town = data.getString("town").trim();
			landlord = data.getString("landlord").trim();
			fields = new JsonArray().add(id).add(name).add(street).add(town).add(landlord);

			break;

		case "retrieve":
			
			funcSP = "sp_getEstate";
			landlord = data.getString("landlord").trim();
			fields = new JsonArray().add(landlord);

			break;

		case "retrieveEstate":
			
			funcSP = "sp_getEstateById";
			id = data.getString("id").trim();
			fields = new JsonArray().add(id);

			break;

		case "retrieveAllEstates":

			funcSP = "sp_getAllEstates";
			fields = new JsonArray().add(operation);

			break;

		case "delete":
			
			funcSP = "sp_removeEstate";
			String estate = data.getString("id").trim();
			landlord = data.getString("landlord").trim();
			fields = new JsonArray().add(estate).add(landlord);

			break;

		default:
			System.out.println("No such operation: " + operation);
			break;
		}
		EventBus esbBus = vertx.eventBus();
        callDatabase(message, data, operation, fields, funcSP);
    }


    /**
     * Sends building details to database adapter
     *
     * @param message
     * @param data
     * @param operation
     * @param fields
     * @param funcSP
     */
    private void callDatabase(Message<JsonObject> message, JsonObject data, String operation, JsonArray fields,
                              String funcSP) {
        JsonObject authData = new JsonObject();
        authData.put("storedprocedure", funcSP);
        authData.put("params", fields);

        EventBus esbBus = vertx.eventBus();
        DeliveryOptions options = new DeliveryOptions();
        int time = 5000;
        options.setSendTimeout(time);
        MessageProducer<JsonObject> producer = esbBus.publisher("DATABASEACCESS", options);
        producer.send(authData, (AsyncResult<Message<JsonObject>> artxn) -> {
            if (artxn.succeeded()) {
                JsonObject responseFields = artxn.result().body();
                System.out.println("Received reply from Session Mgr: " + responseFields);

                JsonObject feedback = new JsonObject();
                JsonArray params = responseFields.getJsonArray("params");
                JsonObject dbfields = new JsonObject();
                int param_id = 0;
                int param_size = params.size();
                for (int i = 0; i < param_size; i++) {
                    dbfields = params.getJsonObject(i);
                    if (dbfields.containsKey("id")) {
                        param_id = dbfields.getInteger("id");
                        break;
                    }
                }

                if ((operation.equals("add")) || (operation.equals("update"))) {
                    if (data.containsKey("file")) {
                        String base64 = data.getString("file");
                        String func = "sp_updateEstateFile";

                        new BuildingsMicroService().saveFiles(base64, operation, message, esbBus, param_id, func);
                    }
                    feedback.put("data", params.getJsonObject(0));
                    message.reply(feedback);


                }

                if ((operation.equals("retrieve")) || (operation.equals("retrieveAllEstates"))) {
                    System.out.println("Retrieved data: " + params);

                    feedback.put("data", params);
                    message.reply(feedback);

                } else {
                    System.out.println("Retrieved data: " + params);

                    feedback.put("data", params.getJsonObject(0));
                    message.reply(feedback);

                }

            } else {
                message.fail(0, artxn.cause().getMessage());

            }

        });
    }
}