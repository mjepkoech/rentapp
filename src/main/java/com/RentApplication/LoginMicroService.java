package com.RentApplication;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.reactivex.ext.auth.User;

import javax.xml.bind.DatatypeConverter;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.time.ZonedDateTime;
import java.util.Date;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import static javax.crypto.Cipher.SECRET_KEY;

/**
 * Verifies login credentials and adds tokens
 *
 * @author mjepkoech
 *
 */
public class LoginMicroService extends AbstractVerticle {
    private static final Logger logger = LoggerFactory.getLogger(APIGateway.class);
    private static final String JWT_TOKEN_KEY = "supersecret";
    public static JWTAuth jwtauth = null;

	@Override
	public void start() throws Exception {
		EventBus eb = vertx.eventBus();

		MessageConsumer<JsonObject> consumer = eb.consumer("login");
		consumer.handler((Message<JsonObject> message) -> {
			JsonObject reqdata = message.body();

			System.out.println("LoginMicroservice received a message: " + reqdata);
			try {
				JsonObject data = reqdata.getJsonObject("data").getJsonObject("transaction_details");

                JsonArray login_fields = new JsonArray();

                String func = "sp_userLogin";

				String mobilenumber = data.getString("mobilenumber").trim();
				String password = data.getString("password").trim();

                login_fields = new JsonArray().add(mobilenumber).add(password);
                System.out.println("my fields=" + login_fields);

				JsonObject authData = new JsonObject();
                authData.put("storedprocedure", func);
                authData.put("params", login_fields);

				EventBus esbBus = vertx.eventBus();
				DeliveryOptions options = new DeliveryOptions();
				int time = 5000;
				options.setSendTimeout(time);
				MessageProducer<JsonObject> producer = esbBus.publisher("DATABASEACCESS", options);
				producer.send(authData, (AsyncResult<Message<JsonObject>> artxn) -> {
					if (artxn.succeeded()) {

						JsonObject responseFields = artxn.result().body();

						JsonArray params = responseFields.getJsonArray("params");

						JsonObject dbfields = new JsonObject();
						String username = "";
						int param_size = params.size();
						for (int i = 0; i < param_size; i++) {
							dbfields = params.getJsonObject(i);
							if (dbfields.containsKey("name")) {
								username = dbfields.getString("name");
								break;

							}
						}
						// Set default config
						JsonObject jwt = new JsonObject().put("keyStore", new JsonObject().put("path", "keystore.jceks")
								.put("type", "jceks").put("password", "secret"));
						JWTAuth jwtconfig = JWTAuth.create(vertx, jwt);

//						jwtauth = JWTAuth.create(vertx, getJWTAuthOptions());


						String newtoken = jwtconfig.generateToken(
								new JsonObject().put("sub", data.getString("mobilenumber")).put("name", username),
                                new JWTOptions().setExpiresInMinutes(600));

                        Integer id = responseFields.getJsonArray("params").getJsonObject(0).getInteger("id");
                        System.out.println("Received reply from Manager: " + id + newtoken);

                        savetokentoDB(new JsonArray().add(id).add(newtoken), message);

						responseFields.getJsonArray("params").getJsonObject(0).put("token", newtoken);

						System.out.println("Received reply from Session Manager: " + responseFields);

						JsonObject feedback = new JsonObject();
						feedback.put("data", params.getJsonObject(0));

						message.reply(feedback);
					} else {
						message.fail(0, artxn.cause().getMessage());

					}

				});
			} catch (Exception ex) {
				message.fail(0, ex.getMessage());
			}

		});

		consumer.completionHandler(res -> {
			if (res.succeeded()) {
				System.out.println("Login handler has reached all nodes");
			} else {
				System.out.println("Login failed!");
			}
		});
	}


    private String generateToken(User user) {

        try {
            Algorithm algorithm = Algorithm.HMAC256(JWT_TOKEN_KEY);
            Date expirationDate = Date.from(ZonedDateTime.now().plusHours(24).toInstant());
            Date issuedAt = Date.from(ZonedDateTime.now().toInstant());
            return JWT.create()
                    // Issue date.
                    .withIssuedAt(issuedAt)
                    // Expiration date.
                    .withExpiresAt(expirationDate)
                    // User id - here we can put anything we want, but for the example userId is appropriate.
                    .withClaim("userId", 35)
                    // Issuer of the token.
                    .withIssuer("jwtauth")
                    // And the signing algorithm.
                    .sign(algorithm);
        } catch (UnsupportedEncodingException | JWTCreationException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }


    private User validateToken(String token) {
        try {
            if (token != null) {
                Algorithm algorithm = Algorithm.HMAC256(JWT_TOKEN_KEY);
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("jwtauth")
                        .build(); //Reusable verifier instance
                DecodedJWT jwt = verifier.verify(token);
                //Get the userId from token claim.
                Claim userId = jwt.getClaim("userId");
                // Find user by token subject(id).
//				UserDao userDao = new UserDao();
//				return userDao.findUserById(userId.asLong());
            }
        } catch (UnsupportedEncodingException | JWTVerificationException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * take the generated jwt token and the user it was generated for and save it in
     * the database
     *
     * @param fields
     * @param message
     */
    private void savetokentoDB(JsonArray fields, Message<JsonObject> message) {
        EventBus esbBus = vertx.eventBus();

        String operation = "add";
        String funcSP = "sp_loginToken";

        new DatabaseService().callDatabase(funcSP, fields, operation, message, esbBus);
    }


    /**
     * configure JWT defaults
     *
     * @return
     */
    private JWTAuthOptions getJWTAuthOptions() {

        String keystorepath = "keystore.jceks";
        String password = "secret";
        String keystoretype = "jceks";

        JsonObject authconfig = new JsonObject();
        authconfig.put("type", keystoretype);
        authconfig.put("path", keystorepath);
        authconfig.put("password", password);

//		LOGGER.info("JWT Init " + new JWTAuthOptions(new JsonObject().put("keyStore", authconfig)).toString());
        return new JWTAuthOptions(new JsonObject().put("keyStore", authconfig));
    }
}