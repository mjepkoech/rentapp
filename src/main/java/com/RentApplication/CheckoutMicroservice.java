package com.RentApplication;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClientOptions;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.UUID;

import static javax.sound.sampled.FloatControl.Type.PAN;

public class CheckoutMicroservice extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		String conf = "{\n" +
				"    \"pg_c2b_callback_port\": 7453,\n" +
				"    \"pg_c2b_callback_host\": \"10.20.2.4\",\n" +
				"    \"clientid\": \"5064\",\n" +
				"    \"short_code\": \"174379\",\n" +
				"    \"pg_username\": \"easy.rent\",\n" +
				"    \"pg_password\": \"e794e56686bb0a92c1049191d2ddacd581f5ffe43ad0000a3e40429bf8c5fee35ba4389a553e3d884ce8cc3072d8d125f9774eb2a14b2bc50470ea3011c29724\",\n" +
				"    \"pg_rsa_key\": \"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo61xA95Q8+CV56KLdJtpZNMy3bwp7//cFThqR06LUNugOqfUsxmhQ/FKYwvcJHjiwc00/fdO5ygDpRFSd01oqasLRfbL3x1QNhecAFMGuiSkqvFpGqaWrqQQKSZPO/pGgcffCvBqkpEJFdh24U9IVlKnH+ErtnGQxVVsVbLzXAg8aw6B3AoGsMCeosEHJYsDxcLLxwO5Nf173HRh9gC426lENhI7c94fhU+96DGU2aV3mQ4cwSmbm7Wz4o1QBAgCU0kF3qO/0QPtG2iHVJxHelJjkhU1Yp6FL17TjSmwgP09Xnya+lMf8uCeZLzDwWBQN2HeHWuTNZppUB23Agh8CwIDAQAB\",\n" +
				"    \"service_id\": {\n" +
				"      \"mpesa_checkout\": \"5067\",\n" +
				"      \"card_checkout\": \"3038\"\n" +
				"    }\n" +
				"  }";
		JsonObject pgConfig = new JsonObject(conf);


		EventBus eb = vertx.eventBus();

		MessageConsumer<JsonObject> consumer = eb.consumer("paymentCheckout");
		consumer.handler((Message<JsonObject> message) -> {

			JsonObject reqdata = message.body();
			JsonObject data = reqdata.getJsonObject("data").getJsonObject("transaction_details");
			String transaction_type = reqdata.getJsonObject("data").getJsonObject("transaction_details")
					.getString("transaction_type").trim();
			System.out.println("Sent reply from op Mgr: " + transaction_type);

			JsonObject paymentgatewayJsonOBJ = new JsonObject();
			paymentgatewayJsonOBJ.put("amount", data.getString("amount").trim())
					.put("username", pgConfig.getString("pg_username"))
					.put("password", pgConfig.getString("pg_password"))
					.put("clientid", pgConfig.getString("clientid"))
					.put("accountno", data.getString("mobile_number").trim())
					.put("msisdn", data.getString("mobile_number").trim())
					.put("accountreference", data.getString("invoice_id").trim())
					.put("narration", data.getString("invoice_id").trim());

			try {
				JsonArray fields = new JsonArray();
				String funcSP = "";
				switch (transaction_type) {
					case "MPESACHECKOUT":
						funcSP = "sp_saveTransaction";

						String currency = data.getString("currency").trim();
						String amount = data.getString("amount").trim();
						String reference_number = UUID.randomUUID().toString();
						String mobile_number = data.getString("mobile_number").trim();
						String paid_by = data.getString("user_id").trim();
						String password = data.getString("pass").trim();
						String provider_id = data.getString("provider_id").trim();
						String channel = reqdata.getJsonObject("data").getJsonObject("channel_details").getString("channel")
								.trim();
						String invoice_id = data.getString("invoice_id").trim();

						fields = new JsonArray().add(currency).add(amount).add(reference_number)
								.add(mobile_number).add(paid_by).add(password).add(provider_id)
								.add(channel).add(invoice_id);
						System.out.println("Sent reply from Session Mgr: " + fields);

						break;

					case "CARDCHECKOUT":
						funcSP = "sp_saveTransaction";

						currency = data.getString("currency").trim();
						amount = data.getString("amount").trim();
						reference_number = UUID.randomUUID().toString();
						mobile_number = "";
						paid_by = data.getString("user_id").trim();
						password = data.getString("pass").trim();
						provider_id = data.getString("provider_id").trim();
						channel = reqdata.getJsonObject("data").getJsonObject("channel_details").getString("channel")
								.trim();
						invoice_id = data.getString("invoice_id").trim();

						fields = new JsonArray().add(currency).add(amount).add(reference_number)
								.add(mobile_number).add(paid_by).add(password).add(provider_id).add(channel)
								.add(invoice_id);
						break;

					default:
						System.out.println("No such TRANSACTION TYPE: " + transaction_type);
						break;
				}
				JsonObject authData = new JsonObject();
				authData.put("storedprocedure", funcSP);
				authData.put("params", fields);
				System.out.println("my fields=" + authData);

				DeliveryOptions options = new DeliveryOptions();
				int time = 5000;
				options.setSendTimeout(time);
				System.out.println("my fields=" + authData);

				EventBus esbBus = vertx.eventBus();

				MessageProducer<JsonObject> producer = esbBus.publisher("DATABASEACCESS", options);
				producer.send(authData, (AsyncResult<Message<JsonObject>> artxn) -> {
					if (artxn.succeeded()) {
						JsonObject responseFields = artxn.result().body();
						System.out.println("Received reply from Session Mgr: " + responseFields);

						JsonObject feedback = new JsonObject();
						JsonArray params = responseFields.getJsonArray("params");


						// DO THE ACTUAL SENDING

						WebClient client = WebClient.create(vertx, new WebClientOptions()
								.setTrustAll(true));
						if (transaction_type.equals("MPESACHECKOUT")) {
							paymentgatewayJsonOBJ.put("amount", data.getString("amount").trim())
									.put("transactionid", params.getJsonObject(0).getInteger("id").toString())
									.put("serviceid", pgConfig.getJsonObject("service_id").getString("mpesa_checkout"))
									.put("shortcode", pgConfig.getString("short_code"));
							System.out.println("PG RESPE:" + paymentgatewayJsonOBJ);

							client.post(8443, "testgateway.ekenya.co.ke", "/ServiceLayer/onlinecheckout/request")
									.ssl(true).timeout(60000).sendJsonObject(paymentgatewayJsonOBJ, inqRes -> {
								if (inqRes.succeeded()) {
									HttpResponse<io.vertx.core.buffer.Buffer> response = inqRes.result();
									JsonObject pgrawresponse = response.bodyAsJsonObject();
									System.out.println("PG RESPONSE:" + pgrawresponse);
									JsonObject responseOBJ = new JsonObject();
									String	PGMESSAGE = pgrawresponse.getString("STATUSDESCRIPTION")

                                            .replaceAll("[^a-zA-Z0-9 ]", " ");
										params.getJsonObject(0).put("message", PGMESSAGE);

										System.out.println("Payment details =" + responseOBJ);

										feedback.put("data", params.getJsonObject(0));
										message.reply(feedback);
								} else {
									message.fail(0, inqRes.cause().getMessage());
								}
							});
						} else if (transaction_type.equals("CARDCHECKOUT")) {
							JsonObject payload = new JsonObject();
							payload
									.put("txnCurrency", data.getString("currency"))
									.put("ExpiryDate", data.getString("card_expiry_date"))
									.put("PAN", data.getString("card_pan"))
									.put("CardNumber", data.getString("card_number"))
									.put("CardSecurityCode", data.getString("card_security_code"));

							//PG ENCRYPTION
							String encryptedDetails = encryptDetails(payload, pgConfig);

							paymentgatewayJsonOBJ.put("transactionid", params.getJsonObject(0).getInteger("id").toString())
									.put("currencycode", data.getString("currency").trim())
									.put("timestamp", params.getJsonObject(0).getString("created_at"))
									.put("serviceid", pgConfig.getJsonObject("service_id").getString("card_checkout"));

							paymentgatewayJsonOBJ.put("payload", encryptedDetails);
							System.out.println("The card payload: " + paymentgatewayJsonOBJ);
							client.post(8443, "testgateway.ekenya.co.ke", "/ServiceLayer/request/postRequest")
									.ssl(true)
									.timeout(6000).sendJsonObject(paymentgatewayJsonOBJ, inqRes -> {
								if (inqRes.succeeded()) {
									HttpResponse<io.vertx.core.buffer.Buffer> response = inqRes.result();
									JsonObject pgrawresponse = response.bodyAsJsonObject();
									System.out.println("PG RESPONSE:" + pgrawresponse);
									String PGMESSAGE = pgrawresponse.getString("statusDescription")
												.replaceAll("[^a-zA-Z0-9 ]", " ");
									params.getJsonObject(0).put("message", PGMESSAGE);

										feedback.put("data", params.getJsonObject(0));
										message.reply(feedback);
								} else {
									message.fail(0, inqRes.cause().getMessage());
								}
							});
						}
					} else {
						message.fail(0, artxn.cause().getMessage());

					}

				});
			} catch (Exception ex) {
				message.fail(0, ex.getMessage());
			}

		});

		consumer.completionHandler(res -> {
			if (res.succeeded()) {
				System.out.println("MpesaCheckout handler has reached all nodes");
			} else {
				System.out.println("MpesaCheckout handler failed!");
			}
		});
	}

	private static String encryptDetails(JsonObject payload, JsonObject pgconfig) {
		String payloadbase64 = "";

		try {
			Cipher cipher = Cipher.getInstance("RSA");

			byte[] keyBytes = Base64.getDecoder().decode(pgconfig.getString("pg_rsa_key"));
			X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			PublicKey key = kf.generatePublic(spec);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			String payloadbase641 = new String(Base64.getEncoder().encode(cipher.doFinal(payload.toString().getBytes("UTF-8"))));
			payloadbase64 = payloadbase641;
			return payloadbase64;
		} catch (NoSuchAlgorithmException noSuchAlgo) {
			System.out.println(" No Such Algorithm exists " + noSuchAlgo);
		} catch (NoSuchPaddingException noSuchPad) {
			System.out.println(" No Such Padding exists " + noSuchPad);
		} catch (InvalidKeyException invalidKey) {
			System.out.println(" Invalid Key " + invalidKey);
		} catch (InvalidKeySpecException invalidKeySpec) {
			System.out.println(" Invalid Key Spec " + invalidKeySpec);
		} catch (BadPaddingException badPadding) {
			System.out.println(" Bad Padding " + badPadding);
		} catch (UnsupportedEncodingException UnsupportedEncoding) {
			System.out.println(" Unsupported Encoding " + UnsupportedEncoding);
		} catch (IllegalBlockSizeException illegalBlockSize) {
			System.out.println(" Illegal Block Size " + illegalBlockSize);
		}
		return payloadbase64;

	}

}
